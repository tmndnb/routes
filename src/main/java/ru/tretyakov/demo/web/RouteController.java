package ru.tretyakov.demo.web;


import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.tretyakov.demo.service.*;

import java.util.Arrays;

@Controller
@RequiredArgsConstructor
public class RouteController {
    @Autowired
    RoutesService routesService;
    @Autowired
    UrlService urlService;
    @Autowired
    UserService userService;

    @GetMapping("/routes/{id}/delete/{url}")
    public String deleteUrl(@PathVariable("id") Long id, @PathVariable("url") Long url, Model model) {
        UrlDTO urlDTO = urlService.getOne(url);
        if (urlDTO == null) {
            return null;
        }
        urlService.delete(urlDTO.getId());
        RoutesDTO routesOne = routesService.getOne(id);
        model.addAttribute("routesOne", routesOne);
        return "routes";
    }

    @GetMapping("/routes/delete/{id}")
    public String deleteRoutes(@PathVariable Long id) {
        RoutesDTO routesOne = routesService.getOne(id);
        if (routesOne == null) {
            return null;
        }
        routesService.delete(routesOne.getId());
        return "redirect:/dashboard";
    }

    @GetMapping("/routes/{id}")
    public String getRoutes(@PathVariable Long id, Model model) {
        UserDTO userDTO = userService.currentUser();
        RoutesDTO routesOne = routesService.getOne(id);
        model.addAttribute("routesOne", routesOne)
                .addAttribute("user", userDTO);
        return "routes";
    }

    @PostMapping("/routes")
    public String saveOrUpdate(@ModelAttribute("routesOne") RoutesDTO routesOne) {
        RoutesDTO routesDTO;
        if (routesOne.getId() != null) {
            routesDTO = routesService.getOne(routesOne.getId());
        } else {
            RoutesDTO routesNewId = routesService.saveOnlyID(routesOne);
            routesDTO = new RoutesDTO();
            routesDTO.setId(routesNewId.getId());
        }
        routesDTO.setName(routesOne.getName())
                .setDate(routesOne.getDate())
                .setUrl(routesOne.getUrl());
        routesService.saveOrUpdate(routesDTO);
        return "redirect:/dashboard";
    }

    @GetMapping("/routes/add")
    public String addRoutes(Model model) {
        UserDTO userDTO = userService.currentUser();
        RoutesDTO routesOne = new RoutesDTO();
        UrlDTO urlDTO = new UrlDTO();
        routesOne.setUrl(Arrays.asList(urlDTO));
        model.addAttribute("routesOne", routesOne)
                .addAttribute("user", userDTO);
        return "routes";
    }
}
