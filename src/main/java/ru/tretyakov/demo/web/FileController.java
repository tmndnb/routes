package ru.tretyakov.demo.web;

import lombok.RequiredArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ru.tretyakov.demo.service.FileHashDTO;
import ru.tretyakov.demo.service.FileHashService;
import ru.tretyakov.demo.service.UserDTO;
import ru.tretyakov.demo.service.UserService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class FileController {
    @Autowired
    UserService userService;

    @Autowired
    FileHashService fileHashService;

    @GetMapping("/files/download/{id}")
    public ResponseEntity<Resource> downloadFile(@PathVariable Long id) {
        UserDTO userDTO = userService.currentUser();
        FileHashDTO fileDTO = fileHashService.getOne(id, userDTO.getId());
        if (fileDTO == null) {
            return null;
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(fileDTO.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileDTO.getFileName() + "\"")
                .body(new ByteArrayResource(fileDTO.getData()));
    }

    @PostMapping("/files/uploadFile")
    public String submit(@RequestParam("file") MultipartFile file, Model model) throws IOException {
        UserDTO userDTO = userService.currentUser();

//        if (!file.isEmpty()) {
//            byte[] bytes = file.getBytes();
//            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
//            Files.write(path, bytes);
//        }

        String md5Hex = DigestUtils.md5Hex(file.getInputStream());
        String sha256Hex = DigestUtils.sha256Hex(file.getInputStream());

        fileHashService.addFile(file, md5Hex, sha256Hex, userDTO.getId());

        model.addAttribute("file", file)
                .addAttribute("md5Hex", md5Hex)
                .addAttribute("sha256Hex", sha256Hex)
                .addAttribute("user", userDTO);
        return "uploadFile";
    }

    @GetMapping("/files/delete/{id}")
    public String deleteFile(@PathVariable Long id) {
        UserDTO userDTO = userService.currentUser();
        FileHashDTO fileHashDTO = fileHashService.getOne(id, userDTO.getId());
        if (fileHashDTO == null) {
            return null;
        }
        fileHashService.deleteFile(fileHashDTO.getId());
        return "redirect:/files";
    }

    @GetMapping("/files")
    public String getFiles(Model model) {
        UserDTO userDTO = userService.currentUser();
        List<FileHashDTO> listFiles = fileHashService.getAllFileUser(userDTO.getId());
        model.addAttribute("listFiles", listFiles)
                .addAttribute("user", userDTO);
        return "files-list";
    }

    @GetMapping("/files/{id}")
    public String getOneFile(@PathVariable Long id, Model model) {
        UserDTO userDTO = userService.currentUser();
        FileHashDTO oneFile = fileHashService.getOne(id, userDTO.getId());
        model.addAttribute("oneFile", oneFile)
                .addAttribute("user", userDTO);
        return "file-one";
    }

    @GetMapping("/files/search/{type}")
    public String searchFile(@PathVariable String type, @RequestParam("hash") String hash, Model model) {
        UserDTO userDTO = userService.currentUser();
        FileHashDTO oneFile = fileHashService.searchHash(hash, userDTO.getId(), type);
        model.addAttribute("oneFile", oneFile)
                .addAttribute("user", userDTO);
        return "file-one";
    }
}
