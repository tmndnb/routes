package ru.tretyakov.demo.web;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.tretyakov.demo.service.*;
import ru.tretyakov.demo.web.form.RegistrationForm;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class WebController {
    @Autowired
    RoutesService routesService;
    @Autowired
    UrlService urlService;
    @Autowired
    UserService userService;
    @Autowired
    FileHashService fileHashService;

    @GetMapping("/")
    public String getIndex() {
        return "index";
    }

    @GetMapping("/dashboard")
    public String getDashboard(Model model) {
        UserDTO userDTO = userService.currentUser();
        List<RoutesDTO> routes = routesService.getAll();
        List<FileHashDTO> allFileUser = fileHashService.getAllFileUser(userDTO.getId());
        model.addAttribute("routes", routes)
                .addAttribute("user", userDTO)
                .addAttribute("allFileUser", allFileUser);
        return "dashboard";
    }

    @GetMapping("/user")
    public String getUser(Model model) {
        UserDTO userDTO = userService.currentUser();
        List<FileHashDTO> allFileUser = fileHashService.getAllFileUser(userDTO.getId());
        model.addAttribute("user", userDTO)
                .addAttribute("allFileUser", allFileUser);
        return "user";
    }

    @GetMapping("/registration")
    public String getRegistration(RegistrationForm form) {
        return "registration";
    }

    @PostMapping("/registration")
    public String postRegistration(@Valid RegistrationForm form, BindingResult result) {
        if (!result.hasErrors()) {
            UserDTO userDTO = userService.registration(form.getFirstName(), form.getLastName(), form.getEmail(), form.getPassword());
            if (userDTO != null) {
                return "redirect:/";
            }
        }
        return "registration";
    }
}
