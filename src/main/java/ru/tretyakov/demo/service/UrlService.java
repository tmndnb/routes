package ru.tretyakov.demo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tretyakov.demo.entity.Url;
import ru.tretyakov.demo.repository.RoutesRepository;
import ru.tretyakov.demo.repository.UrlRepository;

@Service
@RequiredArgsConstructor
public class UrlService {
    @Autowired
    UrlRepository urlRepository;
    @Autowired
    RoutesRepository routesRepository;

    public UrlDTO getOne(Long id) {
        Url one = urlRepository.getOne(id);
        if (one == null) {
            return null;
        }
        return convertToUrlDTO(one);
    }

    public void delete(Long id) {
        urlRepository.deleteById(id);
    }

    public UrlDTO convertToUrlDTO(Url url) {
        return new UrlDTO().setId(url.getId()).setUrlName(url.getUrlName()).setStep(url.getStep());
    }

    public Url convertToUrl(UrlDTO urlDTO, RoutesDTO routesDTO) {
        return new Url().setId(urlDTO.getId()).setUrlName(urlDTO.getUrlName()).setStep(urlDTO.getStep())
                .setRoutes(routesRepository.getOne(routesDTO.getId()));
    }
}
