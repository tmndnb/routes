package ru.tretyakov.demo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.tretyakov.demo.entity.FileHash;
import ru.tretyakov.demo.entity.User;
import ru.tretyakov.demo.repository.FileHashRepository;
import ru.tretyakov.demo.repository.UserRepository;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FileHashService {
    @Autowired
    FileHashRepository fileHashRepository;
    @Autowired
    UserRepository userRepository;

    public List<FileHashDTO> getAllFileUser(Long userId) {
        List<FileHash> listFiles = fileHashRepository.findByUserId(userId);
        if (listFiles.isEmpty()) {
            return new ArrayList<>();
        }
        return listFiles.stream()
                .map(this::convertToFileHashDTO)
                .collect(Collectors.toList());
    }

    public FileHashDTO getOne(Long fileId, Long userId) {
        FileHash one = fileHashRepository.getOne(fileId);
        User user = userRepository.getOne(userId);
        if (one == null) {
            return null;
        }
        if (!one.getUser().equals(user)) {
            return null;
        }
        return convertToFileHashDTO(one);
    }

    public FileHashDTO searchHash(String hash, Long userId, String type) {
        User user = userRepository.getOne(userId);
        FileHash fileHash = null;
        if (type.equals("md5")) {
            fileHash = fileHashRepository.findByMd5AndUser(hash, user);
        }
        if (type.equals("sha256")) {
            fileHash = fileHashRepository.findBySha256AndUser(hash, user);
        }
        if (fileHash == null) {
            return null;
        }
        return convertToFileHashDTO(fileHash);
    }

    public FileHashDTO addFile(MultipartFile file, String md5, String sha256, Long userId) throws IOException {
        FileHash fileHash = new FileHash();
        fileHash.setFileName(file.getOriginalFilename());
        fileHash.setFileType(file.getContentType());
        fileHash.setMd5(md5);
        fileHash.setSha256(sha256);
        fileHash.setSize(file.getSize());
        fileHash.setTime(LocalDateTime.now());
        fileHash.setData(file.getBytes());
        fileHash.setUser(userRepository.getOne(userId));
        FileHash save = fileHashRepository.save(fileHash);
        return convertToFileHashDTO(save);
    }

    public void deleteFile(Long id) {
        fileHashRepository.deleteById(id);
    }

    public FileHashDTO convertToFileHashDTO(FileHash fileHash) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        String uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/files/download/")
                .path(fileHash.getId().toString())
                .toUriString();

        return new FileHashDTO()
                .setId(fileHash.getId())
                .setFileName(fileHash.getFileName())
                .setFileType(fileHash.getFileType())
                .setSha256(fileHash.getSha256())
                .setMd5(fileHash.getMd5())
                .setSize(fileHash.getSize())
                .setFileDownloadUri(uri)
                .setTime(formatter.format(fileHash.getTime()))
                .setData(fileHash.getData())
                .setUserID(fileHash.getUser().getId());
    }

}
