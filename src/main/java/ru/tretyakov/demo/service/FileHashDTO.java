package ru.tretyakov.demo.service;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class FileHashDTO {
    private Long id;
    private String fileName;
    private String fileType;
    private String sha256;
    private String md5;
    private Long size;
    private Long userID;
    private String time;
    private String fileDownloadUri;
    private byte[] data;
}
