package ru.tretyakov.demo.service;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UrlDTO {
    private Long id;
    private String urlName;
    private String step;
}
