package ru.tretyakov.demo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.tretyakov.demo.entity.User;
import ru.tretyakov.demo.repository.UserRepository;
import ru.tretyakov.demo.security.CustomUserDetails;
import ru.tretyakov.demo.security.UserRole;

import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserDTO getOne(long id) {
        User user = userRepository.getOne(id);
        if (user == null) {
            return null;
        }
        return convertToUserDTO(user);
    }

    public UserDTO registration(String firstName, String lastName, String email, String password) {
        Set<UserRole> userRoles = new HashSet<>();
        userRoles.add(UserRole.USER);
        String hash = passwordEncoder.encode(password);
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setPassword(hash);
        user.setRoles(userRoles);
        User saveUser = userRepository.save(user);
        if (saveUser == null) {
            return null;
        }
        return convertToUserDTO(saveUser);
    }


    public UserDTO convertToUserDTO(User user) {
        return new UserDTO().setId(user.getId())
                .setFirstName(user.getFirstName())
                .setLastName(user.getLastName())
                .setEmail(user.getEmail());
    }

    public User converToUser(UserDTO userDTO) {
        return new User().setId(userDTO.getId());
    }

    public UserDTO currentUser() {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userRepository.getOne(customUserDetails.getId());
        return convertToUserDTO(user);
    }
}
