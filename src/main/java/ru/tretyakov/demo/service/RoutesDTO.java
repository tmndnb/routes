package ru.tretyakov.demo.service;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class RoutesDTO {
    private Long id;
    private String name;
    private String date;
    private List<UrlDTO> url;
}
