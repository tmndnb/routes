package ru.tretyakov.demo.service;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserDTO {
    private long id;
    private String firstName;
    private String lastName;
    private String email;
}
