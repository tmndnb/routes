package ru.tretyakov.demo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tretyakov.demo.entity.Routes;
import ru.tretyakov.demo.repository.RoutesRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RoutesService {
    @Autowired
    RoutesRepository routesRepository;
    @Autowired
    UrlService urlService;

    public List<RoutesDTO> getAll() {
        List<Routes> list = routesRepository.findAll();
        if (list.isEmpty()) {
            return new ArrayList<>();
        }
        return list.stream().map(this::convertToRouteDTO).collect(Collectors.toList());
    }

    public RoutesDTO getOne(long id) {
        Routes routes = routesRepository.getOne(id);
        return convertToRouteDTO(routes);
    }

    public RoutesDTO saveOrUpdate(RoutesDTO routesDTO) {
        Routes routes = convertToRoute(routesDTO);
        return convertToRouteDTO(routesRepository.save(routes));
    }

    public RoutesDTO saveOnlyID(RoutesDTO routesDTO) {
        Routes routes = convertToRoutesID(routesDTO);
        Routes save = routesRepository.save(routes);
        return new RoutesDTO().setId(save.getId());
    }

    public void delete(long id) {
        routesRepository.deleteById(id);
    }

    private RoutesDTO convertToRouteDTO(Routes routes) {
        return new RoutesDTO().setId(routes.getId())
                .setName(routes.getName())
                .setDate(routes.getDate())
                .setUrl(routes.getUrl().stream()
                        .map(item -> urlService.convertToUrlDTO(item))
                        .collect(Collectors.toList()));
    }

    private Routes convertToRoute(RoutesDTO routesDTO) {
        return new Routes().setId(routesDTO.getId())
                .setName(routesDTO.getName())
                .setDate(routesDTO.getDate())
                .setUrl(routesDTO.getUrl().stream()
                        .map(item -> urlService.convertToUrl(item, routesDTO))
                        .collect(Collectors.toList()));
    }

    private Routes convertToRoutesID(RoutesDTO routesDTO) {
        return new Routes().setId(routesDTO.getId());
    }
}
