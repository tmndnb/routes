package ru.tretyakov.demo.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Table(name = "url")
@Data
@Accessors(chain = true)
public class Url {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column(name = "url_name")
    private String urlName;

    @Column(name = "step")
    private String step;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "routes_id", referencedColumnName = "id")
    private Routes routes;

    @Override
    public String toString() {
        return "Url{" +
                "id=" + id +
                ", urlName='" + urlName + '\'' +
                ", step='" + step + '\'' +
                '}';
    }
}
