package ru.tretyakov.demo.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "file_hash")
@Data
public class FileHash {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "file_type")
    private String fileType;

    @Column(name = "sha256")
    private String sha256;

    @Column(name = "md5")
    private String md5;

    @Column(name = "size")
    private Long size;

    @Column(name = "times")
    private LocalDateTime time;

    @Column(name = "data")
    private byte[] data;

    @ManyToOne
    @JoinColumn
    private User user;

    @Column(length = 16000000)
    @Basic(fetch = FetchType.LAZY)
    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
