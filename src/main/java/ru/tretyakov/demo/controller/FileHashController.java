//package ru.tretyakov.demo.controller;
//
//import lombok.RequiredArgsConstructor;
//import org.apache.commons.codec.digest.DigestUtils;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MultipartFile;
//import ru.tretyakov.demo.json.FileHashResponse;
//import ru.tretyakov.demo.service.FileHashService;
//
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.List;
//import java.util.Random;
//
//import static org.springframework.http.ResponseEntity.ok;
//import static org.springframework.http.ResponseEntity.status;
//
//@RestController
//@RequestMapping("/api/file_hashes")
//@RequiredArgsConstructor
//public class FileHashController {
//    private final FileHashService fileHashService;
//
//    @GetMapping("/{hash}")
//    public ResponseEntity<List<FileHashResponse>> getHash(@PathVariable String hash) {
//        List<FileHashResponse> list = fileHashService.getByHash(hash);
//        if (list.isEmpty()) {
//            return status(HttpStatus.NOT_FOUND).build();
//        }
//        return ok(list);
//    }
//
//    @GetMapping("/delete/{id}")
//    public ResponseEntity<FileHashResponse> deleteHash(@PathVariable Long id) {
//        FileHashResponse response = fileHashService.deleteHash(id);
//        if (response == null) {
//            return status(HttpStatus.NOT_FOUND).build();
//        }
//        return status(HttpStatus.OK).build();
//    }
//
//    @PostMapping("/add")
//    public ResponseEntity<FileHashResponse> addHash(HttpServletResponse response, @RequestHeader HttpHeaders auth, @RequestParam("file") MultipartFile file) throws IOException {
////        UUID uuid = UUID.randomUUID();
////        String uudiString = uuid.toString();
//        Random random = new Random();
//        int i = random.nextInt(1000);
//        String uudiString = "user=" + i;
//        if (auth.getFirst("AAA") == null) {
//            response.addHeader("AAA", uudiString);
//        }
//
//
//
////        HttpSession session = request.getSession();
////        session.setAttribute("AAA", uuid.toString());
//
//
//        String md5Hex = DigestUtils.md5Hex(file.getInputStream());
//        String sha256Hex = DigestUtils.sha256Hex(file.getInputStream());
//
//        List<FileHashResponse> byHashMd5 = fileHashService.getByHash(md5Hex);
//        if (!byHashMd5.isEmpty()) {
//            return status(HttpStatus.OK).build();
//        }
//        List<FileHashResponse> byHashSha256 = fileHashService.getByHash(sha256Hex);
//        if (!byHashSha256.isEmpty()) {
//            return status(HttpStatus.OK).build();
//        }
//        fileHashService.addFile(uudiString, file.getOriginalFilename(), sha256Hex, md5Hex);
//        return status(HttpStatus.CREATED).build();
//      }
//
////    public boolean auth(HttpServletResponse response, HttpServletRequest request) {
////        request.getHeader("X-Auth-User")
////    }
//
//}
