package ru.tretyakov.demo.security;

public enum UserRole {
    USER,
    ADMIN
}
