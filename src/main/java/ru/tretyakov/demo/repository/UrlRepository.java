package ru.tretyakov.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tretyakov.demo.entity.Url;

@Repository
public interface UrlRepository extends JpaRepository<Url, Long> {
}
