package ru.tretyakov.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tretyakov.demo.entity.Routes;
@Repository
public interface RoutesRepository extends JpaRepository<Routes, Long> {

}
