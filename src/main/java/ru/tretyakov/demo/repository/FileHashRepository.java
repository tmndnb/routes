package ru.tretyakov.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.tretyakov.demo.entity.FileHash;
import ru.tretyakov.demo.entity.User;

import java.util.List;

@Repository
public interface FileHashRepository extends JpaRepository<FileHash, Long> {
    List<FileHash> findByUserId(Long id);

    FileHash findBySha256AndUser(String sha256, User user);

    FileHash findByMd5AndUser(String md5, User user);
}
