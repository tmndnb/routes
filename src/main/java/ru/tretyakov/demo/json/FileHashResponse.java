package ru.tretyakov.demo.json;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FileHashResponse {
    private Long id;
    private String userId;
    private String fileName;
    private String sha526;
    private String md5;


}
